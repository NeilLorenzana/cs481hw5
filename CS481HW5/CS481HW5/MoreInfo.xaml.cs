﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481HW5.Models;

namespace CS481HW5
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfo : ContentPage
	{
		public MoreInfo (Productdatum selectedItem)
		{
			InitializeComponent ();
            BindingContext = selectedItem;

        }
	}
}