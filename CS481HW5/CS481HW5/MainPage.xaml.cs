﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using CS481HW5.Models;


namespace CS481HW5
{
    public partial class MainPage : ContentPage
    {
        Prod prodDataFromJson = new Prod();

        public MainPage()
        {
            InitializeComponent();
            ReadInJsonFile();
        }

        private void ReadInJsonFile()
        {
            var fileName = "CS481HW5.products.json";


            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(fileName);


            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();
                prodDataFromJson = JsonConvert.DeserializeObject<Prod>(jsonAsString);
                
            }

            ProductsListView.ItemsSource = new ObservableCollection<Productdatum>(prodDataFromJson.Productdata);

        }
        private async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var itemClicked = menuItem.CommandParameter as Productdatum;
            await Navigation.PushAsync(new MoreInfo(itemClicked));
        }
    }
}
